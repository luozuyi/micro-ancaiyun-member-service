package com.ancaiyun.controller;

import com.ancaiyun.entity.OrderReceiveAddress;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.service.OrderReceiveAddressService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderReceiveAddressController {
    @Autowired
    private OrderReceiveAddressService orderReceiveAddressService;

    /**
     * 添加收获地址
     * @param token 凭据
     * @param orderReceiveAddress
     * @param provincesCitysCountrys
     * orderReceiveAddress.getName() 收货人姓名*
     * orderReceiveAddress.getTelephone() 收货人电话*
     * provincesCitysCountrys.getProvincesId() 省id*
     * provincesCitysCountrys.getProvincesName() 省名称*
     * provincesCitysCountrys.getCitysId() 市id *
     * provincesCitysCountrys.getCitysName() 市名称 *
     * provincesCitysCountrys.getCountrysId() 区id*
     * provincesCitysCountrys.getCountrysName() 区名称*
     * @return
     */
    @RequestMapping(value = "v1/auth/order-receive-addresss",method = RequestMethod.POST)
    public Result addOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys){
        return orderReceiveAddressService.addOrderReceiveAddress(token, orderReceiveAddress, provincesCitysCountrys);
    }

    /**
     * 查询收货地址详情
     * @param token 凭据
     * @param orderReceiveAddressId 收货地址id
     * @return
     */
    @RequestMapping(value = "v1/auth/order-receive-addresss/id",method = RequestMethod.GET)
    public Result selectOrderReceiveAddressByPrimaryKey(String token, String orderReceiveAddressId){
        return orderReceiveAddressService.selectOrderReceiveAddressByPrimaryKey(token, orderReceiveAddressId);
    }

    /**
     *修改收货地址
     * @param token 凭据
     * @param orderReceiveAddress
     * @param provincesCitysCountrys
     * orderReceiveAddress.getName() 收货人姓名*
     * orderReceiveAddress.getTelephone() 收货人电话*
     * provincesCitysCountrys.getProvincesId() 省id*
     * provincesCitysCountrys.getProvincesName() 省名称*
     * provincesCitysCountrys.getCitysId() 市id *
     * provincesCitysCountrys.getCitysName() 市名称 *
     * provincesCitysCountrys.getCountrysId() 区id*
     * provincesCitysCountrys.getCountrysName() 区名称*
     * @return
     */
    @RequestMapping(value = "v1/auth/order-receive-addresss",method = RequestMethod.PATCH)
    public Result updateOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys) {
        return orderReceiveAddressService.updateOrderReceiveAddress(token, orderReceiveAddress, provincesCitysCountrys);
    }

    /**
     * 设置为默认收货地址
     * @param token 凭据
     * @param orderReceiveAddressId 要设置的地址id
     * @return
     */
    @RequestMapping(value = "v1/auth/order-receive-addresss/usual-address",method = RequestMethod.PATCH)
    public Result updateOrderReceiveAddressUsualAddress(String token, String orderReceiveAddressId){
        return orderReceiveAddressService.updateOrderReceiveAddressUsualAddress(token, orderReceiveAddressId);
    }

    /**
     * 删除地址
     * @param token 凭据
     * @param orderReceiveAddressId 要删除的地址id
     * @return
     */
    @RequestMapping(value = "v1/auth/order-receive-addresss/id",method = RequestMethod.DELETE)
    public Result updateDelFlag(String token, String orderReceiveAddressId){
        return orderReceiveAddressService.updateDelFlag(token, orderReceiveAddressId);
    }
}

