package com.ancaiyun.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping(value = "/sayMyService")
    public String sayMyService(){
        return "hello member service";
    }
}
