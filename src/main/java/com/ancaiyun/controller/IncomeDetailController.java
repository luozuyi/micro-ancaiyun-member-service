package com.ancaiyun.controller;

import com.ancaiyun.entity.IncomeDetail;
import com.ancaiyun.service.IncomeDetailService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IncomeDetailController {
    @Autowired
    private IncomeDetailService incomeDetailService;

    /**
     * 添加收支明细
     * @param token 凭据
     * @param incomeDetail
     * 主键
     *id;
     *
     * 创建时间
     *createTime;
     * 删除状态0：正常1：删除2：待删除
     *delFlag;
     * 用户id
     *userId;
     *
     * 收支场景： 收支场景  0：充值 1：商城订单支付2：自营订单支付3:商城订单确认收货4：自营订单确认收货   5:提现 6:提现审核失败退回
     *flunFlow;
    /**
     * 资金类型  0:余额
     *moneyType;
    /**
     * 财务类型 0:收入 1:支出
     *financeType;
    /**
     * 金额
     *num;
     * @return
     */
    @RequestMapping(value = "v1/auth/income-details",method = RequestMethod.POST)
    public Result addIncomeDetail(String token, IncomeDetail incomeDetail) {
        return incomeDetailService.addIncomeDetail(token, incomeDetail);
    }

    /**
     * 分页查询用户的收支明细
     * @param token 凭据
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @RequestMapping(value = "v1/auth/income-details/pagination",method = RequestMethod.GET)
    public Result selectIncomeDetailPageListByUserId(String token, @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum, Integer pageSize){
        return incomeDetailService.selectIncomeDetailPageListByUserId(token, pageNum, pageSize);
    }
}
