package com.ancaiyun.controller;

import com.ancaiyun.entity.ProductCategoryRelevance;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.entity.UserExtraData;
import com.ancaiyun.service.UserExtraDataService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserExtraDataController {
    @Autowired
    private UserExtraDataService userExtraDataService;

    /**
     * 添加用户额外资料
     * @param token 凭据
     * @param userExtraData 用户额外资料
     * @return
     * companyName 公司名称 *必填
     * userExtraData.getCompanyAnnualTurnover() 每年营业额 *必填
     * userExtraData.getCompanyEnterpriseNature() 单位性质 *必填
     * userExtraData.getCompanyBusinessScope() 经营范围
     * userExtraData.getCompanyBusinessLicenseRegisterDate() 注册成立日期 *必填
     * userExtraData.getCompanyEmployeeNumber() 员工人数 *必填
     *
     * 一类id
     *productCategoryId;
     *
     * 一类名称
     *productCategoryName;
     *
     * 二类id
     *productSubCategoryId;
     *
     * 二类名称
     *productSubCategoryName;
     *
     * 三类id
     *productThirdCategoryId;
     *
     * 三类名称
     *productThirdCategoryName;
     */
    @RequestMapping(value = "v1/auth/user-extra-datas",method = RequestMethod.POST)
    public Result addUserExtraData(String token, UserExtraData userExtraData,ProvincesCitysCountrys provincesCitysCountrys,ProductCategoryRelevance productCategoryRelevance){
        return userExtraDataService.addUserExtraData(token, userExtraData, provincesCitysCountrys,productCategoryRelevance);
    }

    /**
     * 查询用户资料
     * @param token 凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/user-extra-datas/user/id",method = RequestMethod.GET)
    public Result findUserExtraDataByUserId(String token){
        return userExtraDataService.findUserExtraDataByUserId(token);
    }

    /**
     * 修改资料
     * @param token 凭据
     * @param userExtraData
     * @param provincesCitysCountrys
     * @param productCategoryRelevance
     * @return
     * /**
     * 添加用户额外资料
     * @param token 凭据
     * @param userExtraData 用户额外资料
     * @return
     * companyName 公司名称 *必填
     * userExtraData.getCompanyAnnualTurnover() 每年营业额 *必填
     * userExtraData.getCompanyEnterpriseNature() 单位性质 *必填
     * userExtraData.getCompanyBusinessScope() 经营范围
     * userExtraData.getCompanyBusinessLicenseRegisterDate() 注册成立日期 *必填
     * userExtraData.getCompanyEmployeeNumber() 员工人数 *必填
     *
     * 一类id
     *productCategoryId;
     *
     * 一类名称
     *productCategoryName;
     *
     * 二类id
     *productSubCategoryId;
     *
     * 二类名称
     *productSubCategoryName;
     *
     * 三类id
     *productThirdCategoryId;
     *
     * 三类名称
     *productThirdCategoryName;
     *
     */
    @RequestMapping(value = "v1/auth/user-extra-datas",method = RequestMethod.PATCH)
    public Result updateUserExtraData(String token,UserExtraData userExtraData, ProvincesCitysCountrys provincesCitysCountrys, ProductCategoryRelevance productCategoryRelevance){
        return userExtraDataService.updateUserExtraData(token, userExtraData, provincesCitysCountrys, productCategoryRelevance);
    }
}
