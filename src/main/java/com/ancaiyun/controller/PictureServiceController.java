package com.ancaiyun.controller;

import com.ancaiyun.service.PictureService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
public class PictureServiceController {
    @Autowired
    private PictureService pictureService;

    /**
     * 上传文件测试
     * @param file 文件
     * @return
     */
    @RequestMapping(value = "uploadPicTest")
    public Result uploadPicTest(MultipartFile file) throws IOException {
        File fileTest = new File("d:\\015.jpg");
        //file = (MultipartFile)fileTest;
        FileInputStream inputStream = new FileInputStream(fileTest);
        MultipartFile multipartFile = new MockMultipartFile(fileTest.getName(), inputStream);
        return  pictureService.uploadPic(multipartFile);
    }

    /**
     * 上传附件
     * @param token 凭据
     * @param file 文件
     * @return
     */
    @RequestMapping(value = "v1/auth/accessorys",method = RequestMethod.POST)
    public Result uploadPic(String token,MultipartFile file){
        return pictureService.uploadPic(token, file);
    }
}
