package com.ancaiyun.controller;

import com.ancaiyun.entity.FreezeDetail;
import com.ancaiyun.service.FreezeDetailService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FreezeDetailController {
    @Autowired
    private FreezeDetailService freezeDetailService;

    /**
     * 添加冻结明细
     * @param token 凭据
     * @param freezeDetail
     * /**
     * 关联用户id
     *
     * private String userId;
     *
     * 冻结金额
     *private BigDecimal freezeMoney;
     *
     * 冻结类型 0:提现1:商城订单2：自营订单
     *private String freezeType;
     *
     * 冻结状态： 0 冻结中 1 已解冻
     *private String status;
     *
     * 审核人
     *private String approveMan;
     *
     * 审核时间
     *private Date approveTime;
     * @return
     */
    @RequestMapping(value = "v1/auth/freeze-details", method = RequestMethod.POST)
    public Result addFreezeDetail(String token,FreezeDetail freezeDetail) {
        return freezeDetailService.addFreezeDetail(token, freezeDetail);
    }

    /**
     * 解冻资金明细
     * @param token 凭据
     * @param freezeDetailId 解除冻结的对象id
     * @return
     */
    @RequestMapping(value = "v1/auth/freeze-details/freezing",method = RequestMethod.PATCH)
    public Result updateStatusRelease(String token, String freezeDetailId){
        return freezeDetailService.updateStatusRelease(token, freezeDetailId);
    }

    /**
     * 分页查询个人冻结明细
     * @param token 凭据
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @RequestMapping(value = "v1/auth/freeze-details/pagination",method = RequestMethod.GET)
    public Result selectFreezeDetailPageList(String token, @RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum, Integer pageSize) {
        return freezeDetailService.selectFreezeDetailPageList(token, pageNum, pageSize);
    }
}
