package com.ancaiyun.controller;

import com.ancaiyun.entity.Job;
import com.ancaiyun.service.JobService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobController {
    @Autowired
    private JobService jobService;

    /**
     * 新增岗位
     * @param token 凭据
     * @param job
     * jobName 岗位名称
     * @return
     */
    @RequestMapping(value = "v1/auth/jobs",method = RequestMethod.POST)
    public Result addJob(String token, Job job){
        return jobService.addJob(token,job );
    }

    /**
     * 查询当前主用户下的岗位列表
     * @param token 凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/jobs",method = RequestMethod.GET)
    public Result getList(String token){
        return jobService.getListByUserId(token);
    }

    /**
     * 修改岗位名称
     * @param token 凭据
     * @param jobId 要修改的岗位id
     * @param jobName 要修改的岗位名称
     * @return
     */
    @RequestMapping(value = "v1/auth/jobs/job-name",method = RequestMethod.PATCH)
    public Result updateJobName(String token, String jobId,String jobName){
        return jobService.updateJobName(token, jobId, jobName);
    }

    /**
     * 删除岗位
     * @param token 凭据
     * @param jobId 岗位id
     * @return
     */
    @RequestMapping(value = "v1/auth/jobs/id",method = RequestMethod.DELETE)
    public Result updateDelFlagIsDel(String token, String jobId){
        return jobService.updateDelFlagIsDel(token, jobId);
    }
}
