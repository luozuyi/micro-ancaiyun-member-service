package com.ancaiyun.controller;

import com.ancaiyun.entity.User;
import com.ancaiyun.service.UserService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 获取用户信息
     * @param token 用户凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/users",method = RequestMethod.GET)
    public Result findUserByToken(String token){
        return userService.selectByToken(token);
    }

    /**
     *用户注册
     * @param username 用户名
     * @param password 密码
     * @param surePassword 确认密码
     * @param phoneCode 手机验证码
     * @param telphone 手机号
     * @param tradeStatus 供应商状态
     * @param request
     * @return
     */
    @RequestMapping(value = "v1/users",method = RequestMethod.POST)
    public Result userRegist(String username, String password, String surePassword, String phoneCode, String telphone,String tradeStatus, HttpServletRequest request){
        return userService.userRegist(username,password,surePassword,phoneCode,telphone,tradeStatus,request);
    }

    /**
     * 用户用户名校验
     * @param username 用户名
     * @return
     */
    @RequestMapping(value = "v1/users/username",method = RequestMethod.GET)
    public Result checkUsername(String username){
        return userService.checkUsername(username);
    }

    /**
     * 手机号校验
     * @param telphone 手机号
     * @return
     */
    @RequestMapping(value = "v1/users/telphone",method = RequestMethod.GET)
    public Result checkTelphone(String telphone){
        return userService.checkTelphone(telphone);
    }

    /**
     * 修改密码
     * @param token 凭据
     * @param password 密码
     * @param surePassword 确认密码
     * @param oldPassword 旧密码
     * @return
     */
    @RequestMapping(value = "v1/auth/users/password",method = RequestMethod.PATCH)
    public Result updatePassword(String token, String password, String surePassword, String oldPassword){
        return userService.updatePassword(token,password ,surePassword ,oldPassword );
    }

    /**
     *修改支付密码
     * @param token 凭据
     * @param payPassword 支付密码
     * @param surePayPassword 确认支付密码
     * @param oldPayPassword 旧支付密码
     * @return
     */
    @RequestMapping(value = "v1/auth/users/pay-password",method = RequestMethod.PATCH)
    public Result updatePayPassword(String token, String payPassword, String surePayPassword, String oldPayPassword){
        return userService.updatePayPassword(token,payPassword ,surePayPassword ,oldPayPassword );
    }

    /**
     * 发送手机号验证码
     * @param telphone 手机号
     * @param captchaResult 手机号验证码
     * @return
     */
    @RequestMapping(value = "v1/users/telphone-code",method = RequestMethod.GET)
    public Result sendTelphoneCode(String telphone,String captchaResult,HttpServletRequest request){
        return userService.sendTelphoneCode(telphone, captchaResult,request);
    }

    /**
     * 添加子用户
     * @param token 凭据
     * @param childUser
     * @return
     * childUser.getUsername() 子用户名 *必填
     * childUser.getMobile() 手机号 *必填
     * childUser.getPassword()) 密码 *必填
     * childUser.getJobName() 岗位名称 *必填
     * childUser.getOrganizationId() 子账户所属机构Id *必填
     */
    @RequestMapping(value = "v1/auth/users/child-user",method = RequestMethod.POST)
    public Result addChildUser(String token, User childUser){
        return userService.addChildUser(token, childUser);
    }

    /**
     * 分页测试
     * @param pageNum 页数
     * @param pageSize 一页显示多少条
     * @return
     */
    @RequestMapping(value = "v1/users/page-test",method = RequestMethod.GET)
    public Result getListPageTest(@RequestParam(value="pageNum",defaultValue="1")Integer pageNum, Integer pageSize,HttpServletRequest request){
        System.out.println("request.getRequestURL():"+request.getRequestURL());
        System.out.println("request.getRequestURI():"+request.getRequestURI());
        return userService.getListPageTest(pageNum, pageSize);
    }

    /**
     * 主账号禁用子账号
     * @param token 凭据
     * @param userId 要修改的用户主键
     * @param status 修改状态 2：为禁用
     * @return
     */
    @RequestMapping(value = "v1/auth/users/forbidden-child-user",method = RequestMethod.PATCH)
    public Result forbiddenUser(String token, String userId,String status){
        return userService.updateUserStatusIsForbidden(token, userId, status);
    }

    /**
     * 主账号解除禁用子账号
     * @param token 凭据
     * @param userId 要修改的用户主键
     * @param status 修改状态 0：为启用
     * @return
     */
    @RequestMapping(value = "v1/auth/users/forbidden-release-child-user",method = RequestMethod.PATCH)
    public Result forbiddenReleaseUser(String token, String userId,String status){
        return userService.updateUserStatusIsForbiddenRelease(token, userId, status);
    }

    /**
     * 主账号删除子账号
     * @param token 凭据
     * @param userId 要删除的用户主键
     * @return
     */
    @RequestMapping(value = "v1/auth/users/child-user",method = RequestMethod.DELETE)
    public Result deleteChildUser(String token, String userId){
        return userService.updateUserDelFlagIsDelete(token, userId);
    }

    /**
     * 扣除账户可用余额
     * @param token 凭据
     * @param deductMoney 减少的钱
     * @param userId 要减少的钱的用户
     * @param payPassword 支付密码
     * @return
     */
    @RequestMapping(value = "v1/auth/users/deduction-user-money",method = RequestMethod.PATCH)
    public Result updateDeductUserMoney(String token, BigDecimal deductMoney, String userId, String payPassword){
        return userService.updateDeductUserMoney(token, deductMoney, userId, payPassword);
    }
}
