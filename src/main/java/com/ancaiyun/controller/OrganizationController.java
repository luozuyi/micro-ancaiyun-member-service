package com.ancaiyun.controller;

import com.ancaiyun.entity.Organization;
import com.ancaiyun.service.OrganizationService;
import com.ancaiyun.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrganizationController {
    @Autowired
    private OrganizationService organizationService;

    /**
     *添加一级机构
     * @param token 凭据
     * @param organization
     * name 机构名称
     * @return
     */
    @RequestMapping(value = "v1/auth/organizations/organization-first",method = RequestMethod.POST)
    public Result addOrganizationFirst(String token, Organization organization){
        return organizationService.addOrganizationFirst(token,organization);
    }

    /**
     * 添加子机构
     * @param token
     * @param organization
     * name 机构名称
     * parentId 父机构id
     * @return
     */
    @RequestMapping(value = "v1/auth/organizations/child-organization",method = RequestMethod.POST)
    public Result addChildOrganization(String token, Organization organization){
        return organizationService.addChildOrganization(token,organization);
    }

    /**
     * 通过父机构id查询子机构列表
     * @param parentId 机构父id
     *  @param  token 凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/organizations/child-organization",method = RequestMethod.GET)
    public Result getChildOrganizationListByParent(String token,String parentId){
        return organizationService.selectChildOrganizationByParent(token,parentId);
    }

    /**
     * 查询当前用户下的主机构
     * @param token 凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/organizations/parent-organization",method = RequestMethod.GET)
    public Result selectParentOrganizationByUserId(String token){
        return organizationService.selectParentOrganizationByUserId(token);
    }

    /**
     *分页查询该机构下面的子机构
     * @param parentId 当前用户下的主机构
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param token 凭据
     * @return
     */
    @RequestMapping(value = "v1/auth/organizations/child-organization/pagination/parent",method = RequestMethod.GET)
    public Result selectPageChildOrganizationByParent(String token, String parentId, @RequestParam(value="pageNum",defaultValue="1")Integer pageNum, Integer pageSize){
        return organizationService.selectPageChildOrganizationByParent(token, parentId, pageNum, pageSize);
    }
}
