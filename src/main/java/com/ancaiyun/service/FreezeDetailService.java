package com.ancaiyun.service;

import com.ancaiyun.entity.FreezeDetail;
import com.ancaiyun.utils.Result;

public interface FreezeDetailService {
    Result addFreezeDetail(String token,FreezeDetail freezeDetail);//添加冻结明细

    Result updateStatusRelease(String token,String freezeDetailId);//解冻

    Result selectFreezeDetailPageList(String token,Integer pageNum,Integer pageSize);//分页查询冻结明细
}
