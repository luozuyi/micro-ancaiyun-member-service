package com.ancaiyun.service;

import com.ancaiyun.entity.OrderReceiveAddress;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.utils.Result;

public interface OrderReceiveAddressService {
    Result addOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys);

    Result selectOrderReceiveAddressByPrimaryKey(String token, String orderReceiveAddressId);

    Result updateOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys);

    Result updateOrderReceiveAddressUsualAddress(String token, String orderReceiveAddressId);

    Result updateDelFlag(String token, String orderReceiveAddressId);
}
