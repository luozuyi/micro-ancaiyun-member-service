package com.ancaiyun.service;

import com.ancaiyun.entity.Accessory;
import com.ancaiyun.utils.Result;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {
    Result uploadPic(MultipartFile file);

    Result uploadPic(String token,MultipartFile file);
}
