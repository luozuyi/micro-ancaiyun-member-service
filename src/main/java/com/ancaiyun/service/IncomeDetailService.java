package com.ancaiyun.service;

import com.ancaiyun.entity.IncomeDetail;
import com.ancaiyun.utils.Result;

public interface IncomeDetailService {
    Result addIncomeDetail(String token, IncomeDetail incomeDetail);//添加收支明细

    Result selectIncomeDetailPageListByUserId(String token,Integer pageNum,Integer pageSize);
}
