package com.ancaiyun.service;

import com.ancaiyun.entity.Job;
import com.ancaiyun.utils.Result;

public interface JobService {
    Result addJob(String token, Job job);//添加岗位

    Result getListByUserId(String token);//获取当前用户的岗位集合

    Result updateJobName(String token,String jobId,String jobName);//修改岗位名称

    Result updateDelFlagIsDel(String token,String jobId);//删除岗位名称
}
