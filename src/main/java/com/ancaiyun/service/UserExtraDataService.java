package com.ancaiyun.service;

import com.ancaiyun.entity.ProductCategoryRelevance;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.entity.UserExtraData;
import com.ancaiyun.utils.Result;

public interface UserExtraDataService {
    Result addUserExtraData(String token, UserExtraData userExtraData, ProvincesCitysCountrys provincesCitysCountrys, ProductCategoryRelevance productCategoryRelevance);

    Result findUserExtraDataByUserId(String token);

    Result updateUserExtraData(String token,UserExtraData userExtraData, ProvincesCitysCountrys provincesCitysCountrys, ProductCategoryRelevance productCategoryRelevance);
}
