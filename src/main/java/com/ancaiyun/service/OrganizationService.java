package com.ancaiyun.service;

import com.ancaiyun.entity.Organization;
import com.ancaiyun.utils.Result;

public interface OrganizationService {
    Result addOrganizationFirst(String token, Organization organization);//添加主机构

    Result addChildOrganization(String token, Organization organization);//添加子机构

    Result selectChildOrganizationByParent(String token,String parentId);//父机构id查询子机构列表

    Result selectParentOrganizationByUserId(String token);//主账户id查询子机构

    Result selectPageChildOrganizationByParent(String token,String parentId, Integer pageNum, Integer pageSize);//父机构id分页查询子机构列表
}
