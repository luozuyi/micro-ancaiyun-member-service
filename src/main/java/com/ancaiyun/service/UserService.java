package com.ancaiyun.service;

import com.ancaiyun.entity.User;
import com.ancaiyun.utils.Result;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

public interface UserService {

    Result selectByToken(String token);

    Result userRegist(String username, String password, String surePassword, String phoneCode, String telphone,String tradeStatus, HttpServletRequest request);

    Result checkUsername(String username);

    Result checkTelphone(String telphone);

    Result updatePassword(String token, String password, String surePassword, String oldPassword);//修改密码

    Result updatePayPassword(String token, String payPassword, String surePayPassword, String oldPayPassword);//修改支付密码

    Result sendTelphoneCode(String telphone,String captchaResult,HttpServletRequest request);//注册时发送短信验证码，在发送时要图形验证码验证

    Result addChildUser(String token,User user);//添加子账户

    Result getListPageTest(Integer pageNum,Integer pageSize);//分页测试

    Result updateUserStatusIsForbidden(String token,String userId,String status);//禁用子账号

    Result updateUserStatusIsForbiddenRelease(String token,String userId,String status);//接触禁用子账号

    Result updateUserDelFlagIsDelete(String token,String userId);//删除子账号

    Result updateDeductUserMoney(String token,BigDecimal deductMoney,String userId,String payPassword);//用户花销钱
}
