package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.Job;
import com.ancaiyun.mapper.JobMapper;
import com.ancaiyun.service.JobService;
import com.ancaiyun.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class JobServiceImpl implements JobService{
    @Autowired
    private JobMapper jobMapper;
    @Override
    public Result addJob(String token, Job job) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(job.getJobName())){
                code = "-3";
                msg = "岗位名称名称不能为空";
            }else{
                String userId = token.split("_")[1];
                job.setId(UUID.randomUUID().toString().replace("-", ""));
                job.setCreateTime(new Date());
                job.setDelFlag("0");
                job.setUserId(userId);
                Job job_db = jobMapper.selectByPrimaryKeyAndJobName(job);
                if(job_db != null){
                    code = "-4";
                    msg = "已有设置相同名称的岗位";
                }else{
                    jobMapper.insertSelective(job);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result getListByUserId(String token) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            List<Job> jobList = jobMapper.selectByUserId(userId);
            result.setData(jobList);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateJobName(String token, String jobId,String jobName) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            Job job = jobMapper.selectByPrimaryKey(jobId);
            if(job == null){
                code = "-2";
                msg = "修改对象不存在";
            }else if(!userId.equals(job.getUserId())){
                code = "-3";
                msg = "不属于该用户id的岗位";
            }else{
                job.setJobName(jobName);
                jobMapper.updateJobNameByPrimaryKey(job);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateDelFlagIsDel(String token, String jobId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            Job job = jobMapper.selectByPrimaryKey(jobId);
            if(job == null){
                code = "-2";
                msg = "修改对象不存在";
            }else if(!userId.equals(job.getUserId())){
                code = "-3";
                msg = "不属于该用户id的岗位";
            }else{
                job.setDelFlag("1");
                jobMapper.updateDelFlagByPrimaryKey(job);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
