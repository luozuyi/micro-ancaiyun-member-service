package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.IncomeDetail;
import com.ancaiyun.mapper.IncomeDetailMapper;
import com.ancaiyun.service.IncomeDetailService;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class IncomeDetailServiceImpl implements IncomeDetailService{
    @Autowired
    private IncomeDetailMapper incomeDetailMapper;
    @Override
    public Result addIncomeDetail(String token, IncomeDetail incomeDetail) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(incomeDetail.getMoneyType())){
                code = "-3";
                msg = "资金类型不能为空";
            }else if(StringUtils.isBlank(incomeDetail.getFinanceType())){
                code = "-4";
                msg = "财务类型 0:收入 1:支出";
            }else if(incomeDetail.getNum() == null){
                code = "-5";
                msg = "资金大小不能为空";
            }else if(StringUtils.isBlank(incomeDetail.getFlunFlow())){
                code = "-6";
                msg = "请选择收支场景";
            }else {
                String userId = token.split("_")[1];
                incomeDetail.setId(UUID.randomUUID().toString().replace("-", ""));
                incomeDetail.setCreateTime(new Date());
                incomeDetail.setDelFlag("0");
                incomeDetail.setUserId(userId);
                incomeDetailMapper.insertSelective(incomeDetail);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result selectIncomeDetailPageListByUserId(String token, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            PageHelperNew.startPage(pageNum, pageSize);
            List<IncomeDetail> incomeDetailList = incomeDetailMapper.selectAllByUserId(userId);
            PageInfo<IncomeDetail> page = new PageInfo<>(incomeDetailList);
            result.setData(page);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
