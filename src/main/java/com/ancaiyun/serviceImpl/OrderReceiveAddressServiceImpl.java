package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.OrderReceiveAddress;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.mapper.OrderReceiveAddressMapper;
import com.ancaiyun.mapper.ProvincesCitysCountrysMapper;
import com.ancaiyun.service.OrderReceiveAddressService;
import com.ancaiyun.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

@Transactional
@Service
public class OrderReceiveAddressServiceImpl implements OrderReceiveAddressService{
    @Autowired
    private OrderReceiveAddressMapper orderReceiveAddressMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Override
    public Result addOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(orderReceiveAddress.getName())){
                code = "-3";
                msg = "收货人姓名不能为空";
            }else if(StringUtils.isBlank(orderReceiveAddress.getTelephone())){
                code = "-4";
                msg = "收货人电话不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-5";
                msg = "省id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-6";
                msg = "省名称不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-7";
                msg = "市id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())){
                code = "-8";
                msg = "市名称不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCountrysId())){
                code = "-9";
                msg = "区id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCountrysName())){
                code = "-10";
                msg = "区名称不能为空";
            }else{
                String userId = token.split("_")[1];
                String provincesCitysCountrysId = UUID.randomUUID().toString().replace("-", "");
                provincesCitysCountrys.setId(provincesCitysCountrysId);
                provincesCitysCountrys.setCreateTime(new Date());
                provincesCitysCountrys.setDelFlag("0");
                provincesCitysCountrysMapper.insertSelective(provincesCitysCountrys);
                /*
                常用地址
                 */
                if("0".equals(orderReceiveAddress.getUsualAddress())){
                    List<OrderReceiveAddress> orderReceiveAddressList_db = orderReceiveAddressMapper.selectByUserId(userId);
                    for (OrderReceiveAddress orderReceiveAddress_db:orderReceiveAddressList_db) {
                        orderReceiveAddress_db.setUsualAddress("1");
                        orderReceiveAddressMapper.updateUsualAddressByPrimaryKey(orderReceiveAddress_db);
                    }
                    orderReceiveAddress.setProvincesCitysCountrysId(provincesCitysCountrysId);
                    orderReceiveAddress.setUserId(userId);
                    orderReceiveAddressMapper.insertSelective(orderReceiveAddress);
                }else{
                    orderReceiveAddress.setProvincesCitysCountrysId(provincesCitysCountrysId);
                    orderReceiveAddress.setUserId(userId);
                    orderReceiveAddressMapper.insertSelective(orderReceiveAddress);
                }
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result selectOrderReceiveAddressByPrimaryKey(String token, String orderReceiveAddressId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
           if(StringUtils.isBlank(orderReceiveAddressId)){
               code = "-3";
               msg = "请选择查询对象";
           }else{
               OrderReceiveAddress orderReceiveAddress_db = orderReceiveAddressMapper.selectByPrimaryKey(orderReceiveAddressId);
               if(orderReceiveAddress_db == null){
                   code = "-4";
                   msg = "不存在该地址";
               }else{
                   String orderReceiveAddressProvincesCitysCountrysId = orderReceiveAddress_db.getProvincesCitysCountrysId();
                   ProvincesCitysCountrys provincesCitysCountrys = provincesCitysCountrysMapper.selectByPrimaryKey(orderReceiveAddressProvincesCitysCountrysId);
                   Map<String,Object> map = new HashMap<String, Object>();
                   map.put("orderReceiveAddress_db", orderReceiveAddress_db);
                   map.put("provincesCitysCountrys", provincesCitysCountrys);
                   result.setData(map);
                   code = "0";
                   msg = "成功";
               }
           }

        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateOrderReceiveAddress(String token, OrderReceiveAddress orderReceiveAddress, ProvincesCitysCountrys provincesCitysCountrys) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(orderReceiveAddress.getName())){
                code = "-3";
                msg = "收货人姓名不能为空";
            }else if(StringUtils.isBlank(orderReceiveAddress.getTelephone())){
                code = "-4";
                msg = "收货人电话不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-5";
                msg = "省id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-6";
                msg = "省名称不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-7";
                msg = "市id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())){
                code = "-8";
                msg = "市名称不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCountrysId())){
                code = "-9";
                msg = "区id不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCountrysName())){
                code = "-10";
                msg = "区名称不能为空";
            }else{
                String userId = token.split("_")[1];
                /*
                常用地址
                 */
                if("0".equals(orderReceiveAddress.getUsualAddress())){
                    List<OrderReceiveAddress> orderReceiveAddressList_db = orderReceiveAddressMapper.selectByUserId(userId);
                    for (OrderReceiveAddress orderReceiveAddress_db:orderReceiveAddressList_db) {
                        orderReceiveAddress_db.setUsualAddress("1");
                        orderReceiveAddressMapper.updateUsualAddressByPrimaryKey(orderReceiveAddress_db);
                    }
                }
                OrderReceiveAddress orderReceiveAddress_db_wait = orderReceiveAddressMapper.selectByPrimaryKey(orderReceiveAddress.getId());
                orderReceiveAddress_db_wait.setUsualAddress("0");
                orderReceiveAddress_db_wait.setName(orderReceiveAddress.getName());
                orderReceiveAddress_db_wait.setTelephone(orderReceiveAddress.getTelephone());
                orderReceiveAddress_db_wait.setPostcode(orderReceiveAddress.getPostcode());
                orderReceiveAddressMapper.updateByPrimaryKeySelective(orderReceiveAddress_db_wait);
                String provincesCitysCountrysId = orderReceiveAddress_db_wait.getProvincesCitysCountrysId();
                ProvincesCitysCountrys provincesCitysCountrys_db = provincesCitysCountrysMapper.selectByPrimaryKey(provincesCitysCountrysId);
                provincesCitysCountrys_db.setProvincesName(provincesCitysCountrys.getProvincesName());
                provincesCitysCountrys_db.setProvincesId(provincesCitysCountrys.getProvincesId());
                provincesCitysCountrys_db.setCountrysName(provincesCitysCountrys.getCountrysName());
                provincesCitysCountrys_db.setCitysId(provincesCitysCountrys.getCitysId());
                provincesCitysCountrys_db.setCountrysId(provincesCitysCountrys.getCountrysId());
                provincesCitysCountrys_db.setCitysName(provincesCitysCountrys.getCitysName());
                provincesCitysCountrys_db.setAddress(provincesCitysCountrys.getAddress());
                provincesCitysCountrysMapper.updateByPrimaryKeySelective(provincesCitysCountrys_db);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateOrderReceiveAddressUsualAddress(String token, String orderReceiveAddressId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(orderReceiveAddressId)){
                code = "-3";
                msg = "请选择要设置的地址";
            }else{
                String userId = token.split("_")[1];
                List<OrderReceiveAddress> orderReceiveAddressList_db = orderReceiveAddressMapper.selectByUserId(userId);
                for (OrderReceiveAddress orderReceiveAddress_db:orderReceiveAddressList_db) {
                    orderReceiveAddress_db.setUsualAddress("1");
                    orderReceiveAddressMapper.updateUsualAddressByPrimaryKey(orderReceiveAddress_db);
                }

                OrderReceiveAddress orderReceiveAddress_db_wait = orderReceiveAddressMapper.selectByPrimaryKey(orderReceiveAddressId);
                orderReceiveAddress_db_wait.setUsualAddress("0");
                orderReceiveAddressMapper.updateUsualAddressByPrimaryKey(orderReceiveAddress_db_wait);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateDelFlag(String token, String orderReceiveAddressId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(orderReceiveAddressId)){
                code = "-3";
                msg = "请选择要删除的地址";
            }else{
                String userId = token.split("_")[1];
                OrderReceiveAddress orderReceiveAddress_db_wait = orderReceiveAddressMapper.selectByPrimaryKey(orderReceiveAddressId);
                if(!userId.equals(orderReceiveAddress_db_wait.getUserId())){
                    code = "-4";
                    msg = "不是当前用户的收货地址无法删除";
                }else{
                    orderReceiveAddress_db_wait.setDelFlag("1");
                    orderReceiveAddressMapper.updateDelFlagByPrimaryKey(orderReceiveAddress_db_wait);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
