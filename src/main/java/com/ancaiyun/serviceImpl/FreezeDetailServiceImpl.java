package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.FreezeDetail;
import com.ancaiyun.mapper.FreezeDetailMapper;
import com.ancaiyun.service.FreezeDetailService;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class FreezeDetailServiceImpl implements FreezeDetailService{
    @Autowired
    private FreezeDetailMapper freezeDetailMapper;
    @Override
    public Result addFreezeDetail(String token,FreezeDetail freezeDetail) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(freezeDetail.getFreezeMoney() == null){
                msg = "冻结金额不能为空";
                code = "-3";
            }else if(StringUtils.isBlank(freezeDetail.getFreezeType())){
                msg = "冻结类型不能为空";
                code = "-4";
            }else{
                String userId = token.split("_")[1];
                freezeDetail.setId(UUID.randomUUID().toString().replace("-", ""));
                freezeDetail.setDelFlag("0");
                freezeDetail.setCreateTime(new Date());
                freezeDetail.setUserId(userId);
                freezeDetail.setStatus("0");
                freezeDetailMapper.insertSelective(freezeDetail);
                msg = "成功";
                code = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateStatusRelease(String token, String freezeDetailId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(freezeDetailId)){
                code = "-3";
                msg = "要解冻的资金不存在";
            }else{
                FreezeDetail freezeDetail = freezeDetailMapper.selectByPrimaryKey(freezeDetailId);
                if(freezeDetail == null){
                    code = "-4";
                    msg = "解冻的资金对象不存在";
                }else {
                    freezeDetail.setStatus("1");
                    freezeDetailMapper.updateStatusByPrimaryKey(freezeDetail);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectFreezeDetailPageList(String token, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            PageHelperNew.startPage(pageNum, pageSize);
            List<FreezeDetail> freezeDetailList = freezeDetailMapper.selectAllByUserId(userId);
            PageInfo<FreezeDetail> page = new PageInfo<>(freezeDetailList);
            result.setData(page);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
