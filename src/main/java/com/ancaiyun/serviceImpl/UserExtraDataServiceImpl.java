package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.ProductCategoryRelevance;
import com.ancaiyun.entity.ProvincesCitysCountrys;
import com.ancaiyun.entity.UserExtraData;
import com.ancaiyun.mapper.ProductCategoryRelevanceMapper;
import com.ancaiyun.mapper.ProvincesCitysCountrysMapper;
import com.ancaiyun.mapper.UserExtraDataMapper;
import com.ancaiyun.service.UserExtraDataService;
import com.ancaiyun.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Transactional
@Service
public class UserExtraDataServiceImpl implements UserExtraDataService{
    @Autowired
    private UserExtraDataMapper userExtraDataMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Autowired
    private ProductCategoryRelevanceMapper productCategoryRelevanceMapper;
    @Override
    public Result addUserExtraData(String token, UserExtraData userExtraData, ProvincesCitysCountrys provincesCitysCountrys, ProductCategoryRelevance productCategoryRelevance) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(userExtraData.getCompanyBusinessLicenseElectronicVersionId())) {
                code = "-4";
                msg = "请先上传公司营业执照";
            } else if (StringUtils.isBlank(userExtraData.getCompanyName())) {
                code = "-5";
                msg = "公司名称不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyAnnualTurnover())) {
                code = "-6";
                msg = "每年营业额不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyEnterpriseNature())) {
                code = "-7";
                msg = "单位性质不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyBusinessScope())) {
                code = "-8";
                msg = "经营范围不能为空";
            } else if (userExtraData.getCompanyBusinessLicenseRegisterDate() == null) {
                code = "-9";
                msg = "注册成立日期不能为空";
            } else if (userExtraData.getCompanyEmployeeNumber() == null) {
                code = "-10";
                msg = "员工人数不能为空";
            }else{
                String userId = token.split("_")[1];
                UserExtraData userExtraData_db = userExtraDataMapper.selectByUserId(userId);
                if(userExtraData_db != null){
                    code = "-11";
                    msg = "已提交过审核资料";
                }else{
                    //地址关联
                    String provincesCitysCountrysId = UUID.randomUUID().toString().replace("-", "");
                    provincesCitysCountrys.setId(provincesCitysCountrysId);
                    provincesCitysCountrys.setCreateTime(new Date());
                    provincesCitysCountrys.setDelFlag("0");
                    provincesCitysCountrysMapper.insertSelective(provincesCitysCountrys);

                    String userExtraDataId = UUID.randomUUID().toString().replace("-", "");
                    userExtraData.setId(userExtraDataId);
                    userExtraData.setCreateTime(new Date());
                    userExtraData.setDelFlag("0");
                    userExtraData.setProvincesCitysCountrysId(provincesCitysCountrysId);
                    userExtraDataMapper.insertSelective(userExtraData);

                    productCategoryRelevance.setId(UUID.randomUUID().toString().replace("-", ""));
                    productCategoryRelevance.setCreateTime(new Date());
                    productCategoryRelevance.setDelFlag("0");
                    productCategoryRelevance.setRelationId(userExtraDataId);
                    productCategoryRelevanceMapper.insertSelective(productCategoryRelevance);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result findUserExtraDataByUserId(String token) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];

            UserExtraData userExtraData = userExtraDataMapper.selectByUserId(userId);//用户资料
            String provincesCitysCountrysId = userExtraData.getProvincesCitysCountrysId();//地址表id
            String userExtraDataId = userExtraData.getId();//主键Id
            //地址相关信息对象
            ProvincesCitysCountrys provincesCitysCountrys = provincesCitysCountrysMapper.selectByPrimaryKey(provincesCitysCountrysId);
            //物质类型对象
            ProductCategoryRelevance productCategoryRelevance = productCategoryRelevanceMapper.selectByRelationId(userExtraDataId);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("userExtraData", userExtraData);
            map.put("provincesCitysCountrys", provincesCitysCountrys);
            map.put("productCategoryRelevance", productCategoryRelevance);
            result.setData(map);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateUserExtraData(String token, UserExtraData userExtraData, ProvincesCitysCountrys provincesCitysCountrys, ProductCategoryRelevance productCategoryRelevance) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(userExtraData.getCompanyBusinessLicenseElectronicVersionId())) {
                code = "-4";
                msg = "请先上传公司营业执照";
            } else if (StringUtils.isBlank(userExtraData.getCompanyName())) {
                code = "-5";
                msg = "公司名称不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyAnnualTurnover())) {
                code = "-6";
                msg = "每年营业额不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyEnterpriseNature())) {
                code = "-7";
                msg = "单位性质不能为空";
            } else if (StringUtils.isBlank(userExtraData.getCompanyBusinessScope())) {
                code = "-8";
                msg = "经营范围不能为空";
            } else if (userExtraData.getCompanyBusinessLicenseRegisterDate() == null) {
                code = "-9";
                msg = "注册成立日期不能为空";
            } else if (userExtraData.getCompanyEmployeeNumber() == null) {
                code = "-10";
                msg = "员工人数不能为空";
            } else{
                String userId = token.split("_")[1];
                UserExtraData userExtraData_db = userExtraDataMapper.selectByUserId(userId);
                if(userExtraData_db == null){
                    code = "-11";
                    msg = "修改对象不存在";
                }else{
                    String provincesCitysCountrysId = userExtraData_db.getProvincesCitysCountrysId();//地址表id
                    String userExtraDataId = userExtraData.getId();//主键Id
                    /*
                    地址相关信息对象修改
                     */
                    ProvincesCitysCountrys provincesCitysCountrys_db = provincesCitysCountrysMapper.selectByPrimaryKey(provincesCitysCountrysId);
                    provincesCitysCountrys_db.setAddress(provincesCitysCountrys.getAddress());
                    provincesCitysCountrys_db.setCitysId(provincesCitysCountrys.getCitysId());
                    provincesCitysCountrys_db.setCitysName(provincesCitysCountrys.getCitysName());
                    provincesCitysCountrys_db.setCountrysId(provincesCitysCountrys.getCountrysId());
                    provincesCitysCountrys_db.setCountrysName(provincesCitysCountrys.getCountrysName());
                    provincesCitysCountrys_db.setProvincesId(provincesCitysCountrys.getProvincesId());
                    provincesCitysCountrys_db.setProvincesName(provincesCitysCountrys.getProvincesName());
                    provincesCitysCountrysMapper.updateByPrimaryKeySelective(provincesCitysCountrys_db);
                    /*
                    物质类型对象的修改
                     */
                    ProductCategoryRelevance productCategoryRelevance_db = productCategoryRelevanceMapper.selectByRelationId(userExtraDataId);
                    productCategoryRelevance_db.setProductCategoryId(productCategoryRelevance.getProductCategoryId());
                    productCategoryRelevance_db.setProductCategoryName(productCategoryRelevance.getProductCategoryName());
                    productCategoryRelevance_db.setProductSubCategoryId(productCategoryRelevance.getProductSubCategoryId());
                    productCategoryRelevance_db.setProductSubCategoryName(productCategoryRelevance.getProductSubCategoryName());
                    productCategoryRelevance_db.setProductThirdCategoryId(productCategoryRelevance.getProductThirdCategoryId());
                    productCategoryRelevance_db.setProductThirdCategoryName(productCategoryRelevance.getProductThirdCategoryName());
                    productCategoryRelevanceMapper.updateByPrimaryKeySelective(productCategoryRelevance_db);
                   /*
                   资料对象的修改
                    */
                    userExtraData_db.setCompanyBusinessLicenseElectronicVersionId(userExtraData.getCompanyBusinessLicenseElectronicVersionId());
                    userExtraData_db.setCompanyAddressInfo(userExtraData.getCompanyAddressInfo());
                    userExtraData_db.setCompanyAnnualTurnover(userExtraData.getCompanyAnnualTurnover());
                    userExtraData_db.setCompanyBusinessLicenseAddress(userExtraData.getCompanyBusinessLicenseAddress());
                    userExtraData_db.setCompanyBusinessLicenseIssueDate(userExtraData.getCompanyBusinessLicenseIssueDate());
                    userExtraData_db.setCompanyBusinessLicenseRegisterDate(userExtraData.getCompanyBusinessLicenseRegisterDate());
                    userExtraData_db.setCompanyBusinessScope(userExtraData.getCompanyBusinessScope());
                    userExtraData_db.setCompanyEmployeeNumber(userExtraData.getCompanyEmployeeNumber());
                    userExtraData_db.setCompanyEnterpriseNature(userExtraData.getCompanyEnterpriseNature());
                    userExtraData_db.setCompanyLegalPersonCertificateNumber(userExtraData.getCompanyLegalPersonCertificateNumber());
                    userExtraData_db.setCompanyLegalPersonCertificateTelephone(userExtraData.getCompanyLegalPersonCertificateTelephone());
                    userExtraData_db.setCompanyLegalPersonCertificateType(userExtraData.getCompanyTaxpayerType());
                    userExtraData_db.setCompanyLegalPersonName(userExtraData.getCompanyLegalPersonName());
                    userExtraData_db.setCompanyName(userExtraData.getCompanyName());
                    userExtraData_db.setCompanyRegisteredCapital(userExtraData.getCompanyRegisteredCapital());
                    userExtraData_db.setCompanyTaxpayerType(userExtraData.getCompanyTaxpayerType());
                    userExtraData_db.setCompanyTelephone(userExtraData.getCompanyTelephone());
                    userExtraData_db.setCompanyUnifiedSocialCreditCode(userExtraData.getCompanyUnifiedSocialCreditCode());
                    userExtraData_db.setCompanyWebsite(userExtraData.getCompanyWebsite());
                    userExtraDataMapper.updateByPrimaryKeySelective(userExtraData_db);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
