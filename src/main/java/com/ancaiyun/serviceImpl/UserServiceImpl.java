package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.User;
import com.ancaiyun.mapper.UserMapper;
import com.ancaiyun.service.UserService;
import com.ancaiyun.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Transactional
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;

    @Override
    public Result checkUsername(String username) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(!PatternUtil.patternString(username, "username")){
                code = "-3";
                msg = "格式不正确";
            }else{
                User dbuser_username = userMapper.selectByUsername(username);
                if(dbuser_username != null){
                    code = "-4";
                    msg = "账号已被注册";
                }else{
                    code = "-5";
                    msg = "账号可注册";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result checkTelphone(String telphone) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(!PatternUtil.patternString(telphone, "mobile")){
                code = "-3";
                msg = "手机格式不正确";
            }else{
                User dbuser_telphone = userMapper.selectByTelphone(telphone);
                if(dbuser_telphone != null){
                    code = "-4";
                    msg = "手机号已被注册";
                }else{
                    code = "-5";
                    msg = "手机号可用";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updatePassword(String token, String password, String surePassword, String oldPassword) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(password)){
                code = "-3";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-4";
                msg = "确认密码不能为空";
            }else if(!password.equals(surePassword)){
                code = "-5";
                msg = "两次密码不一致";
            }else if(StringUtils.isBlank(oldPassword)){
                code = "-6";
                msg = "原始密码不能为空";
            }else{
                String userId = token.split("_")[1];
                User user = userMapper.selectByPrimaryKey(userId);
                if(!oldPassword.equals(user.getPassword())){
                    code = "-7";
                    msg = "原始密码不正确";
                }else{
                    user.setPassword(password);
                    userMapper.updateByPrimaryKeySelective(user);
                    code = "0";
                    msg = "成功";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updatePayPassword(String token, String payPassword, String surePayPassword, String oldPayPassword) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(payPassword)){
                code = "-3";
                msg = "支付密码不能为空";
            }else if(StringUtils.isBlank(surePayPassword)){
                code = "-4";
                msg = "确认支付密码不能为空";
            }else if(!payPassword.equals(surePayPassword)){
                code = "-5";
                msg = "两次支付密码不一致";
            }else if(StringUtils.isBlank(oldPayPassword)){
                code = "-6";
                msg = "原始支付密码不能为空";
            }else{
                String userId = token.split("_")[1];
                User user = userMapper.selectByPrimaryKey(userId);
                if(!oldPayPassword.equals(user.getPayPassword())){
                    code = "-7";
                    msg = "原始密码不正确";
                }else{
                    user.setPassword(payPassword);
                    userMapper.updateByPrimaryKeySelective(user);
                    code = "0";
                    msg = "成功";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result sendTelphoneCode(String telphone,String captchaResult,HttpServletRequest request) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(!PatternUtil.patternString(telphone, "mobile")){
                code = "-3";
                msg = "格式不正确";
            }else{
                String res = null;
                String url = "https://captcha.luosimao.com/api/site_verify?"+"api_key=f7602fdf889ca10008bb5c626e783eb0&response="+captchaResult;
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpGet httpGet = new HttpGet(url);
                //得到响应
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity resEntity = response.getEntity();
                if(resEntity != null){
                    res = EntityUtils.toString(resEntity,"utf-8");
                }
                boolean flag = res.contains("success");
                if(flag){
                    Random random = new Random();
                    String phoneCode = "";
                    for (int i = 0; i < 6; i++) {
                        int tempCode = random.nextInt(10);
                        phoneCode += tempCode;
                    }
                    if (phoneCode.length() == 6){
                        SMSSendUtil.send(telphone, phoneCode, "30");
                        HttpSession session = request.getSession();
                        String telphoneCode = telphone + "_" + phoneCode;
                        session.setAttribute("telphoneCode", telphoneCode);
                        session.setMaxInactiveInterval(30 * 60);
                        code = "0";
                        msg = "成功";
                    }
                }else{
                    code = "-4";
                    msg = "图形验证码不正确";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addChildUser(String token, User childUser) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(childUser.getUsername())){
                code = "-3";
                msg = "子账户用户名不能为空";
            }else if(StringUtils.isBlank(childUser.getMobile())){
                code = "-4";
                msg = "子账户手机号不能为空";
            }else if(StringUtils.isBlank(childUser.getPassword())){
                code = "-5";
                msg = "子账户登陆密码不能为空";
            }else if(StringUtils.isBlank(childUser.getJobName())){
                code = "-6";
                msg = "子账户岗位不能为空";
            }else if(StringUtils.isBlank(childUser.getOrganizationId())){
                code = "-7";
                msg = "子账户所属机构不能为空";
            }else{
                User dbUser_username = userMapper.selectByUsername(childUser.getUsername());
                User dbUser_telphone = userMapper.selectByTelphone(childUser.getMobile());
                if(dbUser_username != null){
                    code = "-8";
                    msg = "用户名已经被注册过";
                }else if(dbUser_telphone != null){
                    code = "-9";
                    msg = "手机号已经被注册过";
                }else{
                    String userId = token.split("_")[1];
                    User parentUser = userMapper.selectByPrimaryKey(userId);
                    String tradeStatus = parentUser.getTradeStatus();
                    childUser.setId(UUID.randomUUID().toString().replace("-",""));
                    childUser.setCreateTime(new Date());
                    childUser.setDelFlag("0");
                    childUser.setIsChildUser("1");//1代表是子账号
                    childUser.setTradeStatus(tradeStatus);
                    childUser.setBonusPoints(0L);
                    childUser.setFreezeMoney(new BigDecimal("0"));
                    userMapper.insertSelective(childUser);
                    msg = "成功";
                    code = "0";
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getListPageTest(Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<User> userList = userMapper.selectAll();
            PageInfo<User> page = new PageInfo<>(userList);
            result.setData(page);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateUserStatusIsForbidden(String token, String userId,String status) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(userId)){
                code = "-3";
                msg = "请选则要禁用的对象";
            }else if("2".equals(status)){
                code = "-4";
                msg = "已经禁用过";
            }else{
                String parentUserId = token.split("_")[1];
                User user_db = userMapper.selectByPrimaryKey(userId);
                if(user_db == null){
                    code = "-5";
                    msg = "没有该账号";
                }else if(parentUserId.equals(user_db.getParentId())){
                    code = "-6";
                    msg = "不是本人的子账号无法禁用";
                }else{
                    user_db.setStatus(status);
                    userMapper.updateUserStatusByPrimaryKey(user_db);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateUserStatusIsForbiddenRelease(String token, String userId, String status) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(userId)){
                code = "-3";
                msg = "请选则要解除禁用的对象";
            }else if("0".equals(status)){
                code = "-4";
                msg = "已经启用";
            }else{
                String parentUserId = token.split("_")[1];
                User user_db = userMapper.selectByPrimaryKey(userId);
                if(user_db == null){
                    code = "-5";
                    msg = "没有该账号";
                }else if(parentUserId.equals(user_db.getParentId())){
                    code = "-6";
                    msg = "不是本人的子账号无法启用";
                }else{
                    user_db.setStatus(status);
                    userMapper.updateUserStatusByPrimaryKey(user_db);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateUserDelFlagIsDelete(String token, String userId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(userId)){
                code = "-3";
                msg = "请选则要解除禁用的对象";
            }else{
                String parentUserId = token.split("_")[1];
                User user_db = userMapper.selectByPrimaryKey(userId);
                if(user_db == null){
                    code = "-4";
                    msg = "没有该账号";
                }else if(parentUserId.equals(user_db.getParentId())){
                    code = "-5";
                    msg = "不是本人的子账号无法删除";
                }else if(!"2".equals(user_db.getStatus())){
                    code = "-6";
                    msg = "只有禁用状态的子账号才能被删除";
                }else{
                    userMapper.updateUserDelfalgByPrimaryKey(userId);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result updateDeductUserMoney(String token, BigDecimal deductMoney, String userId, String payPassword) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(deductMoney == null){
                code = "-3";
                msg = "花销金额不能为空";
            }else if(StringUtils.isBlank(userId)){
                code = "-4";
                msg = "花销金额的用户不能为空";
            }else if(StringUtils.isBlank(payPassword)){
                code = "-5";
                msg = "支付密码不能为空";
            }else {
                User user_wait_deduct_money = userMapper.selectByPrimaryKey(userId);
                if(user_wait_deduct_money == null){
                    code = "-6";
                    msg = "花销金额的用户不存在";
                }else if((user_wait_deduct_money.getMoney()).compareTo(deductMoney)==-1){
                    code = "-7";
                    msg = "花销金额的用户不存在";
                }else if(!payPassword.equals(user_wait_deduct_money.getPayPassword())){
                    code = "-8";
                    msg = "支付密码不正确";
                }else {
                    BigDecimal money = user_wait_deduct_money.getMoney().subtract(deductMoney);
                    user_wait_deduct_money.setMoney(money);
                    userMapper.updateUserMoneyByPrimaryKey(user_wait_deduct_money);
                    code = "0";
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result userRegist(String username, String password, String surePassword, String phoneCode, String telphone, String tradeStatus, HttpServletRequest request) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(username)){
                code = "-3";
                msg = "用户命不能为空";
            }else if(StringUtils.isBlank(password)){
                code = "-4";
                msg = "密码不能为空";
            }else if(StringUtils.isBlank(surePassword)){
                code = "-5";
                msg = "确认密码不能为空";
            }else if(StringUtils.isBlank(phoneCode)){
                code = "-6";
                msg = "验证码不能为空";
            }else if(StringUtils.isBlank(telphone)){
                code = "-7";
                msg = "手机号不能为空";
            }else if(!password.equals(surePassword)){
                code = "-8";
                msg = "密码不一致";
            }else if(!PatternUtil.patternString(password,"password")){
                code = "-9";
                msg = "密码格式为字母数字6-16位";
            }else if(StringUtils.isBlank(tradeStatus)){
                code = "-10";
                msg = "选择身份";
            }else{
                String sessionTelphoneCode = request.getSession().getAttribute("telphoneCode")+"";
                if(StringUtils.isBlank(sessionTelphoneCode) || sessionTelphoneCode.equals("null")){
                    code = "-11";
                    msg = "验证码已经过期";
                }else{
                    String sessionTelphone = sessionTelphoneCode.split("_")[0];
                    String sessionPhoneCode = sessionTelphoneCode.split("_")[1];
                    if(!telphone.equals(sessionTelphone)){
                        code = "-12";
                        msg = "发送验证码的手机和提交的手机号不一致";
                    }else if(phoneCode.equals(sessionPhoneCode)){
                        code = "-13";
                        msg = "验证码不正确";
                    }else{
                        User dbUser_username = userMapper.selectByUsername(username);
                        User dbUser_telphone = userMapper.selectByTelphone(telphone);
                        if(dbUser_username != null){
                            code = "-14";
                            msg = "用户名已经被注册过";
                        }else if(dbUser_telphone != null){
                            code = "-15";
                            msg = "手机号已经被注册过";
                        }else{
                            User user = new User();
                            user.setId(UUID.randomUUID().toString().replace("-",""));
                            user.setCreateTime(new Date());
                            user.setDelFlag("0");
                            user.setBonusPoints(0L);
                            user.setUsername(username);
                            user.setFreezeMoney(new BigDecimal("0"));
                            user.setIsChildUser("0");
                            user.setMobile(telphone);
                            user.setPassword(password);
                            user.setSex("0");
                            user.setRegisterIp(CommonUtils.getIpAddr(request));
                            user.setTradeStatus(tradeStatus);
                            user.setStatus("0");
                            userMapper.insertSelective(user);
                            msg = "成功";
                            code = "0";
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectByToken(String token) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[0];
            User user = userMapper.selectByPrimaryKey(userId);
            result.setData(user);
            msg = "成功";
            code = "0";
        } catch (Exception e) {
            e.printStackTrace();
            code = "-2";
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
