package com.ancaiyun.serviceImpl;

import com.ancaiyun.entity.Organization;
import com.ancaiyun.entity.User;
import com.ancaiyun.mapper.OrganizationMapper;
import com.ancaiyun.mapper.UserMapper;
import com.ancaiyun.service.OrganizationService;
import com.ancaiyun.utils.PageHelperNew;
import com.ancaiyun.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class OrganizationServiceImpl implements OrganizationService{
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrganizationMapper organizationMapper;
    @Override
    public Result addOrganizationFirst(String token, Organization organization) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(organization.getName())){
                code = "-3";
                msg = "机构名称不能为空";
            }else if(StringUtils.isNotBlank(organization.getParentId())){
                code = "-4";
                msg = "主机构不能有父";
            }else{
                String userId = token.split("_")[1];
                User user = userMapper.selectByPrimaryKey(userId);
                if(StringUtils.isNotBlank(user.getParentId())){
                    code = "-5";
                    msg = "主账号才能操作";
                }else{
                    organization.setId(UUID.randomUUID().toString().replace("-", ""));
                    organization.setCreateTime(new Date());
                    organization.setDelFlag("0");
                    organization.setUserId(userId);
                    Organization organization_db = organizationMapper.selectByPrimaryKeyAndName(organization);
                    if(organization_db != null){
                        code = "-6";
                        msg = "已有设置相同名称的机构";
                    }else{
                        organizationMapper.insertSelective(organization);
                        code = "0";
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result addChildOrganization(String token, Organization organization) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(organization.getName())){
                code = "-3";
                msg = "机构名称不能为空";
            }else if(StringUtils.isBlank(organization.getParentId())){
                code = "-4";
                msg = "父机构不能为空";
            }else{
                String userId = token.split("_")[1];
                User user = userMapper.selectByPrimaryKey(userId);
                if(StringUtils.isNotBlank(user.getParentId())){
                    code = "-5";
                    msg = "主账号才能操作";
                }else{
                    organization.setId(UUID.randomUUID().toString().replace("-", ""));
                    organization.setCreateTime(new Date());
                    organization.setDelFlag("0");
                    organization.setUserId(userId);
                    Organization organization_db = organizationMapper.selectByPrimaryKeyAndName(organization);
                    if(organization_db != null){
                        code = "-6";
                        msg = "已有设置相同名称的机构";
                    }else{
                        organizationMapper.insertSelective(organization);
                        code = "0";
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result selectChildOrganizationByParent(String token,String parentId) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(parentId)){
                code = "-3";
                msg = "父机构不能为空";
            }else{
                List<Organization> organizationList = organizationMapper.selectChildOrganizationByParent(parentId);
                result.setData(organizationList);
                code = "0";
                msg = "成功";
            }
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result selectParentOrganizationByUserId(String token) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            String userId = token.split("_")[1];
            List<Organization> organizationList = organizationMapper.selectParentOrganizationByUserId(userId);
            result.setData(organizationList);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result selectPageChildOrganizationByParent(String token,String parentId, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = "-1";
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Organization> organizationList = organizationMapper.selectChildOrganizationByParent(parentId);
            PageInfo<Organization> page = new PageInfo<>(organizationList);
            result.setData(page);
            code = "0";
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "系统繁忙";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
