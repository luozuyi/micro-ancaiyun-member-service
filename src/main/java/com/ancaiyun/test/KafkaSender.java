package com.ancaiyun.test;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;

import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@EnableBinding(Source.class)
public class KafkaSender {
    private final Logger logger = LoggerFactory.getLogger(KafkaSender.class);

    @Resource
    private Source source;
    @RequestMapping("sendMessage")
    public String sendMessage(String message) {
        try {
            source.output().send(MessageBuilder.withPayload("message: " + message).build());
        } catch (Exception e) {
            logger.info("消息发送失败，原因："+e);
            e.printStackTrace();
        }
        return "success";
    }
}
