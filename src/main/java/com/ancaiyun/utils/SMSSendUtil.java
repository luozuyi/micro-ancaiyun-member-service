package com.ancaiyun.utils;

import com.ancaiyun.core.sms.AppConfig;
import com.ancaiyun.core.sms.ConfigLoader;
import com.ancaiyun.core.sms.MESSAGEXsend;
import org.apache.commons.lang.StringUtils;

public class SMSSendUtil {
    public static void send(String phone, String code, String time) {
        if (StringUtils.isBlank(phone) || StringUtils.isBlank(code) || StringUtils.isBlank(time)) {
            return;
        }

        AppConfig config = ConfigLoader.load(ConfigLoader.ConfigType.Message);
        MESSAGEXsend submail = new MESSAGEXsend(config);
        submail.addTo(phone);
        submail.setProject("Y8YUd3");
        submail.addVar("code", code);
        submail.addVar("time", time);
        submail.xsend();
    }

    public static void main(String[] args) {
        send("13164138097", "123456", "30");
    }
}
