package com.ancaiyun.intercetpor;

import com.alibaba.fastjson.JSON;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import com.ancaiyun.utils.Result;

public class AddIntercetpor implements HandlerInterceptor {

	@Autowired
	private RestTemplate restTemplate;
	@Override
	public boolean preHandle(HttpServletRequest request,
							 HttpServletResponse response, Object handler) throws Exception {
		response.setContentType("application/json;charset=UTF-8");
		String token=request.getParameter("token");
		String url = request.getRequestURL().toString();
		String uri = request.getRequestURI().toString();
		String method = request.getMethod();
		System.out.println("url:"+url);
		System.out.println("uri:"+uri);
		if(StringUtils.isNotBlank(token)){
			System.out.println("携带token");
			Result result  = restTemplate.getForEntity("http://micro-ancaiyun-zuul/sso/v1/authenticate?token="+token+"&requestUrl="+uri+"&method="+method,Result.class).getBody();
			System.out.println("result:"+result);
			System.out.println("code:"+result.getCode());
			if("0".equals(result.getCode())){
				return true;
			}else{
				//response.getWriter().print(result.getMsg());
				PrintWriter writer = response.getWriter();
				writer.write(JSON.toJSONString(result));
				writer.flush();
				writer.close();
				return false;
			}
		}else{
			System.out.println("未携带token");
			System.out.println("restTemplate"+restTemplate);
			// String json = restTemplate.getForObject("http://micro-ancaiyun-zuul/order/noLogin",String.class);
			//Result result  = restTemplate.getForEntity("http://micro-ancaiyun-zuul/order/noLogin",Result.class).getBody();
			Result result = new Result();
			result.setMsg("未登录");
			result.setCode("-100");
			// response.getWriter().print(json);
			PrintWriter writer = response.getWriter();
			writer.write(JSON.toJSONString(result));
			writer.flush();
			writer.close();
			return false;
		}
	}

	@Override
	public void postHandle(HttpServletRequest request,
						   HttpServletResponse response, Object handler,
						   ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request,
								HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}
	@HystrixCommand(fallbackMethod = "getResultFallback")
	public Result ssoAuth(String url){
		return restTemplate.getForEntity(url,Result.class).getBody();
	}

	public Result getResultFallback(String url){
		Result result = new Result();
		result.setMsg("请求超时");
		result.setCode("-500");
		return result;
	}
}
