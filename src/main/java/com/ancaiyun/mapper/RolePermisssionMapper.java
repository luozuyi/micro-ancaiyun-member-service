package com.ancaiyun.mapper;

import com.ancaiyun.entity.RolePermisssion;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RolePermisssionMapper {
    int deleteByPrimaryKey(String id);

    int insert(RolePermisssion record);

    int insertSelective(RolePermisssion record);

    RolePermisssion selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(RolePermisssion record);

    int updateByPrimaryKey(RolePermisssion record);
}