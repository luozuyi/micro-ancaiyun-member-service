package com.ancaiyun.mapper;

import com.ancaiyun.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectByUsername(String username);

    User selectByTelphone(String mobile);

    List<User> selectAll();

    int updateUserStatusByPrimaryKey(User record);

    int updateUserDelfalgByPrimaryKey(String id);

    int updateUserMoneyByPrimaryKey(User record);
}