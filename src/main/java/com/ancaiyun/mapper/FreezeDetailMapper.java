package com.ancaiyun.mapper;

import com.ancaiyun.entity.FreezeDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FreezeDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(FreezeDetail record);

    int insertSelective(FreezeDetail record);

    FreezeDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(FreezeDetail record);

    int updateByPrimaryKey(FreezeDetail record);

    int updateStatusByPrimaryKey(FreezeDetail record);

    List<FreezeDetail> selectAllByUserId(String userId);
}