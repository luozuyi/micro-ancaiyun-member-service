package com.ancaiyun.mapper;

import com.ancaiyun.entity.ProvincesCitysCountrys;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ProvincesCitysCountrysMapper {
    int deleteByPrimaryKey(String id);

    int insert(ProvincesCitysCountrys record);

    int insertSelective(ProvincesCitysCountrys record);

    ProvincesCitysCountrys selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ProvincesCitysCountrys record);

    int updateByPrimaryKey(ProvincesCitysCountrys record);
}