package com.ancaiyun.mapper;

import com.ancaiyun.entity.UserExtraData;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UserExtraDataMapper {
    int deleteByPrimaryKey(String id);

    int insert(UserExtraData record);

    int insertSelective(UserExtraData record);

    UserExtraData selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserExtraData record);

    int updateByPrimaryKey(UserExtraData record);

    UserExtraData selectByUserId(String userId);
}