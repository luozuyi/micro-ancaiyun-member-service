package com.ancaiyun.mapper;

import com.ancaiyun.entity.Organization;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface OrganizationMapper {
    int deleteByPrimaryKey(String id);

    int insert(Organization record);

    int insertSelective(Organization record);

    Organization selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Organization record);

    int updateByPrimaryKey(Organization record);

    Organization selectByPrimaryKeyAndName(Organization record);

    List<Organization> selectChildOrganizationByParent(String parentId);

    List<Organization> selectParentOrganizationByUserId(String userId);
}