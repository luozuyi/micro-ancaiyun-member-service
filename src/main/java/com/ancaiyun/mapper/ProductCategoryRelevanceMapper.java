package com.ancaiyun.mapper;

import com.ancaiyun.entity.ProductCategoryRelevance;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

@Mapper
@Service
public interface ProductCategoryRelevanceMapper {
    int deleteByPrimaryKey(String id);

    int insert(ProductCategoryRelevance record);

    int insertSelective(ProductCategoryRelevance record);

    ProductCategoryRelevance selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ProductCategoryRelevance record);

    int updateByPrimaryKey(ProductCategoryRelevance record);

    ProductCategoryRelevance selectByRelationId(String relationId);
}