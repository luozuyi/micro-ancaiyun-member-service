package com.ancaiyun.mapper;

import com.ancaiyun.entity.OrderReceiveAddress;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface OrderReceiveAddressMapper {
    int deleteByPrimaryKey(String id);

    int insert(OrderReceiveAddress record);

    int insertSelective(OrderReceiveAddress record);

    OrderReceiveAddress selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(OrderReceiveAddress record);

    int updateByPrimaryKey(OrderReceiveAddress record);

    List<OrderReceiveAddress> selectByUserId(String userId);

    int updateUsualAddressByPrimaryKey(OrderReceiveAddress record);

    int updateDelFlagByPrimaryKey(OrderReceiveAddress record);
}