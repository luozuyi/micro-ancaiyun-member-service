package com.ancaiyun.mapper;

import com.ancaiyun.entity.Job;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface JobMapper {
    int deleteByPrimaryKey(String id);

    int insert(Job record);

    int insertSelective(Job record);

    Job selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Job record);

    int updateByPrimaryKey(Job record);

    Job selectByPrimaryKeyAndJobName(Job record);

    List<Job> selectByUserId(String userId);

    int updateJobNameByPrimaryKey(Job record);

    int updateDelFlagByPrimaryKey(Job record);
}