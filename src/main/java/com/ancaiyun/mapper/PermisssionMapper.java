package com.ancaiyun.mapper;

import com.ancaiyun.entity.Permisssion;

public interface PermisssionMapper {
    int deleteByPrimaryKey(String id);

    int insert(Permisssion record);

    int insertSelective(Permisssion record);

    Permisssion selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Permisssion record);

    int updateByPrimaryKey(Permisssion record);
}