package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

public class ProductCategoryRelevance implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键Id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 一类id
     */
    private String productCategoryId;
    /**
     * 一类名称
     */
    private String productCategoryName;
    /**
     * 二类id
     */
    private String productSubCategoryId;
    /**
     * 二类名称
     */
    private String productSubCategoryName;
    /**
     * 三类id
     */
    private String productThirdCategoryId;
    /**
     * 三类名称
     */
    private String productThirdCategoryName;
    /**
     * 相关联id
     */
    private String relationId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId == null ? null : productCategoryId.trim();
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName == null ? null : productCategoryName.trim();
    }

    public String getProductSubCategoryId() {
        return productSubCategoryId;
    }

    public void setProductSubCategoryId(String productSubCategoryId) {
        this.productSubCategoryId = productSubCategoryId == null ? null : productSubCategoryId.trim();
    }

    public String getProductSubCategoryName() {
        return productSubCategoryName;
    }

    public void setProductSubCategoryName(String productSubCategoryName) {
        this.productSubCategoryName = productSubCategoryName == null ? null : productSubCategoryName.trim();
    }

    public String getProductThirdCategoryId() {
        return productThirdCategoryId;
    }

    public void setProductThirdCategoryId(String productThirdCategoryId) {
        this.productThirdCategoryId = productThirdCategoryId == null ? null : productThirdCategoryId.trim();
    }

    public String getProductThirdCategoryName() {
        return productThirdCategoryName;
    }

    public void setProductThirdCategoryName(String productThirdCategoryName) {
        this.productThirdCategoryName = productThirdCategoryName == null ? null : productThirdCategoryName.trim();
    }

    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId == null ? null : relationId.trim();
    }
}