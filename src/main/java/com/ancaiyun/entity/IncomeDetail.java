package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IncomeDetail implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 收支场景： 收支场景  0：充值 1：商城订单支付2：自营订单支付3:商城订单确认收货4：自营订单确认收货   5:提现 6:提现审核失败退回
     */
    private String flunFlow;
    /**
     * 资金类型  0:余额
     */
    private String moneyType;
    /**
     * 财务类型 0:收入 1:支出
     */
    private String financeType;
    /**
     * 金额
     */
    private BigDecimal num;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getFlunFlow() {
        return flunFlow;
    }

    public void setFlunFlow(String flunFlow) {
        this.flunFlow = flunFlow == null ? null : flunFlow.trim();
    }

    public String getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(String moneyType) {
        this.moneyType = moneyType == null ? null : moneyType.trim();
    }

    public String getFinanceType() {
        return financeType;
    }

    public void setFinanceType(String financeType) {
        this.financeType = financeType == null ? null : financeType.trim();
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }
}