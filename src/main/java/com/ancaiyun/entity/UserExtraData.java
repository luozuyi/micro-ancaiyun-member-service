package com.ancaiyun.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UserExtraData implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态
     */
    private String delFlag;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 注册资金
     */
    private BigDecimal companyRegisteredCapital;
    /**
     * 员工人数0:50人以下1：50-150 2：150-500 3：500-1000 4：1000以上
     */
    private Integer companyEmployeeNumber;
    /**
     * 公司电话
     */
    private String companyTelephone;
    /**
     * 公司所在地
     */
    private String companyAddressInfo;
    /**
     * 单位性质1：生产制造2：贸易批发3：商业服务4：其他
     */
    private String companyEnterpriseNature;
    /**
     * 纳税人类型1一般纳税人2小规模纳税人
     */
    private String companyTaxpayerType;
    /**
     * 每年营业额每年营业额1：100万以下2：100万以上3：1000万-5000万4：5000万-1亿5：1亿-10亿6：10亿以上
     */
    private String companyAnnualTurnover;
    /**
     * 公司详细地址id
     */
    private String provincesCitysCountrysId;
    /**
     * 经营范围1生产2贸易3服务
     */
    private String companyBusinessScope;
    /**
     * 法人姓名
     */
    private String companyLegalPersonName;
    /**
     * 企业网址
     */
    private String companyWebsite;
    /**
     * 法人证件类型0身份证1护照2其他
     */
    private String companyLegalPersonCertificateType;
    /**
     * 法人证件号
     */
    private String companyLegalPersonCertificateNumber;
    /**
     * 法人电话
     */
    private String companyLegalPersonCertificateTelephone;
    /**
     * 统一社会信用代码
     */
    private String companyUnifiedSocialCreditCode;
    /**
     * 发证日期
     */
    private Date companyBusinessLicenseIssueDate;
    /**
     * 注册成立日期
     */
    private Date companyBusinessLicenseRegisterDate;
    /**
     * 营业执照所在地
     */
    private String companyBusinessLicenseAddress;
    /**
     * 营业执照电子版
     */
    private String companyBusinessLicenseElectronicVersionId;
    /**
     * 关联用户id
     */
    private String userId;
    /**
     * 资料审核状态：0：审核中 1：审核通过 2：审核失败
     */
    private String status;
    /**
     * 公司logoid
     */
    private String companyLogoId;
    /**
     * 联系人职位
     */
    private String contactPosition;
    /**
     * 联系人姓名
     */
    private String contactName;
    /**
     * 联系人电话
     */
    private String contactPhone;
    /**
     * 联系人详情
     */
    private String companyInfoDetail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public BigDecimal getCompanyRegisteredCapital() {
        return companyRegisteredCapital;
    }

    public void setCompanyRegisteredCapital(BigDecimal companyRegisteredCapital) {
        this.companyRegisteredCapital = companyRegisteredCapital;
    }

    public Integer getCompanyEmployeeNumber() {
        return companyEmployeeNumber;
    }

    public void setCompanyEmployeeNumber(Integer companyEmployeeNumber) {
        this.companyEmployeeNumber = companyEmployeeNumber;
    }

    public String getCompanyTelephone() {
        return companyTelephone;
    }

    public void setCompanyTelephone(String companyTelephone) {
        this.companyTelephone = companyTelephone == null ? null : companyTelephone.trim();
    }

    public String getCompanyAddressInfo() {
        return companyAddressInfo;
    }

    public void setCompanyAddressInfo(String companyAddressInfo) {
        this.companyAddressInfo = companyAddressInfo == null ? null : companyAddressInfo.trim();
    }

    public String getCompanyEnterpriseNature() {
        return companyEnterpriseNature;
    }

    public void setCompanyEnterpriseNature(String companyEnterpriseNature) {
        this.companyEnterpriseNature = companyEnterpriseNature == null ? null : companyEnterpriseNature.trim();
    }

    public String getCompanyTaxpayerType() {
        return companyTaxpayerType;
    }

    public void setCompanyTaxpayerType(String companyTaxpayerType) {
        this.companyTaxpayerType = companyTaxpayerType == null ? null : companyTaxpayerType.trim();
    }

    public String getCompanyAnnualTurnover() {
        return companyAnnualTurnover;
    }

    public void setCompanyAnnualTurnover(String companyAnnualTurnover) {
        this.companyAnnualTurnover = companyAnnualTurnover == null ? null : companyAnnualTurnover.trim();
    }

    public String getProvincesCitysCountrysId() {
        return provincesCitysCountrysId;
    }

    public void setProvincesCitysCountrysId(String provincesCitysCountrysId) {
        this.provincesCitysCountrysId = provincesCitysCountrysId == null ? null : provincesCitysCountrysId.trim();
    }

    public String getCompanyBusinessScope() {
        return companyBusinessScope;
    }

    public void setCompanyBusinessScope(String companyBusinessScope) {
        this.companyBusinessScope = companyBusinessScope == null ? null : companyBusinessScope.trim();
    }

    public String getCompanyLegalPersonName() {
        return companyLegalPersonName;
    }

    public void setCompanyLegalPersonName(String companyLegalPersonName) {
        this.companyLegalPersonName = companyLegalPersonName == null ? null : companyLegalPersonName.trim();
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite == null ? null : companyWebsite.trim();
    }

    public String getCompanyLegalPersonCertificateType() {
        return companyLegalPersonCertificateType;
    }

    public void setCompanyLegalPersonCertificateType(String companyLegalPersonCertificateType) {
        this.companyLegalPersonCertificateType = companyLegalPersonCertificateType == null ? null : companyLegalPersonCertificateType.trim();
    }

    public String getCompanyLegalPersonCertificateNumber() {
        return companyLegalPersonCertificateNumber;
    }

    public void setCompanyLegalPersonCertificateNumber(String companyLegalPersonCertificateNumber) {
        this.companyLegalPersonCertificateNumber = companyLegalPersonCertificateNumber == null ? null : companyLegalPersonCertificateNumber.trim();
    }

    public String getCompanyLegalPersonCertificateTelephone() {
        return companyLegalPersonCertificateTelephone;
    }

    public void setCompanyLegalPersonCertificateTelephone(String companyLegalPersonCertificateTelephone) {
        this.companyLegalPersonCertificateTelephone = companyLegalPersonCertificateTelephone == null ? null : companyLegalPersonCertificateTelephone.trim();
    }

    public String getCompanyUnifiedSocialCreditCode() {
        return companyUnifiedSocialCreditCode;
    }

    public void setCompanyUnifiedSocialCreditCode(String companyUnifiedSocialCreditCode) {
        this.companyUnifiedSocialCreditCode = companyUnifiedSocialCreditCode == null ? null : companyUnifiedSocialCreditCode.trim();
    }

    public Date getCompanyBusinessLicenseIssueDate() {
        return companyBusinessLicenseIssueDate;
    }

    public void setCompanyBusinessLicenseIssueDate(Date companyBusinessLicenseIssueDate) {
        this.companyBusinessLicenseIssueDate = companyBusinessLicenseIssueDate;
    }

    public Date getCompanyBusinessLicenseRegisterDate() {
        return companyBusinessLicenseRegisterDate;
    }

    public void setCompanyBusinessLicenseRegisterDate(Date companyBusinessLicenseRegisterDate) {
        this.companyBusinessLicenseRegisterDate = companyBusinessLicenseRegisterDate;
    }

    public String getCompanyBusinessLicenseAddress() {
        return companyBusinessLicenseAddress;
    }

    public void setCompanyBusinessLicenseAddress(String companyBusinessLicenseAddress) {
        this.companyBusinessLicenseAddress = companyBusinessLicenseAddress == null ? null : companyBusinessLicenseAddress.trim();
    }

    public String getCompanyBusinessLicenseElectronicVersionId() {
        return companyBusinessLicenseElectronicVersionId;
    }

    public void setCompanyBusinessLicenseElectronicVersionId(String companyBusinessLicenseElectronicVersionId) {
        this.companyBusinessLicenseElectronicVersionId = companyBusinessLicenseElectronicVersionId == null ? null : companyBusinessLicenseElectronicVersionId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getCompanyLogoId() {
        return companyLogoId;
    }

    public void setCompanyLogoId(String companyLogoId) {
        this.companyLogoId = companyLogoId;
    }

    public String getContactPosition() {
        return contactPosition;
    }

    public void setContactPosition(String contactPosition) {
        this.contactPosition = contactPosition;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getCompanyInfoDetail() {
        return companyInfoDetail;
    }

    public void setCompanyInfoDetail(String companyInfoDetail) {
        this.companyInfoDetail = companyInfoDetail;
    }
}