package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

public class OrderReceiveAddress implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0：正常1：删除2：待删除
     */
    private String delFlag;
    /**
     * 收货人姓名
     */
    private String name;
    /**
     * 收货人电话
     */
    private String telephone;
    /**
     * 邮编
     */
    private String postcode;
    /**
     * 是否为常用地址: 0:是 1:否
     */
    private String usualAddress;
    /**
     * 省市区关联id
     */
    private String provincesCitysCountrysId;
    /**
     * 关联用户id
     */
    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode == null ? null : postcode.trim();
    }

    public String getUsualAddress() {
        return usualAddress;
    }

    public void setUsualAddress(String usualAddress) {
        this.usualAddress = usualAddress == null ? null : usualAddress.trim();
    }

    public String getProvincesCitysCountrysId() {
        return provincesCitysCountrysId;
    }

    public void setProvincesCitysCountrysId(String provincesCitysCountrysId) {
        this.provincesCitysCountrysId = provincesCitysCountrysId == null ? null : provincesCitysCountrysId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }
}