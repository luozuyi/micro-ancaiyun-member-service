package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

public class ProvincesCitysCountrys implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态0正常1删除2待删除
     */
    private String delFlag;
    /**
     * 省id
     */
    private String provincesId;
    /**
     * 省名称
     */
    private String provincesName;
    /**
     * 市id
     */
    private String citysId;
    /**
     * 市名称
     */
    private String citysName;
    /**
     * 区id
     */
    private String countrysId;
    /**
     * 区名称
     */
    private String countrysName;
    /**
     * 详细地址
     */
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId == null ? null : provincesId.trim();
    }

    public String getProvincesName() {
        return provincesName;
    }

    public void setProvincesName(String provincesName) {
        this.provincesName = provincesName == null ? null : provincesName.trim();
    }

    public String getCitysId() {
        return citysId;
    }

    public void setCitysId(String citysId) {
        this.citysId = citysId == null ? null : citysId.trim();
    }

    public String getCitysName() {
        return citysName;
    }

    public void setCitysName(String citysName) {
        this.citysName = citysName == null ? null : citysName.trim();
    }

    public String getCountrysId() {
        return countrysId;
    }

    public void setCountrysId(String countrysId) {
        this.countrysId = countrysId == null ? null : countrysId.trim();
    }

    public String getCountrysName() {
        return countrysName;
    }

    public void setCountrysName(String countrysName) {
        this.countrysName = countrysName == null ? null : countrysName.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }
}