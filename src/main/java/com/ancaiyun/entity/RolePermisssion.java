package com.ancaiyun.entity;

import java.io.Serializable;
import java.util.Date;

public class RolePermisssion implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id;

    private Date createTime;

    private String delFlag;

    private String permisssionId;

    private String roleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getPermisssionId() {
        return permisssionId;
    }

    public void setPermisssionId(String permisssionId) {
        this.permisssionId = permisssionId == null ? null : permisssionId.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}